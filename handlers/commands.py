from aiogram.dispatcher import FSMContext
from aiogram.utils.deep_linking import get_start_link

from bot import dp, bot
from config import Config
from aiogram.types import LabeledPrice, ContentType
from aiogram.dispatcher.filters import Command
from aiogram.types import Message, CallbackQuery, PreCheckoutQuery, ShippingQuery, ShippingOption, InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData
from aiogram.dispatcher.filters import Text

from services import DataBase, takeOne, MongoData, helpToChat
from markups import subjectsInline, user_actions, grades, check, changeStatus, courseInline, subjects
from services import help_exit
from states import Subject


db = DataBase('exerDB.sql')
mongoPassword = takeOne(db.getMongoPass())
mDb = MongoData(mongoPassword)

@dp.message_handler(Command('start'))
async def start(message: Message):
    try:
        referalId = message.get_args()
        if referalId != '':
            await db.createUser(message.from_id, message.from_user.full_name, referalId)
        else:
            await db.createUser(message.from_id, message.from_user.full_name)
    except Exception as e:
        pass
    finally:
        await message.answer(Config.sayHi, reply_markup=subjects())
        await message.answer(Config.reference)


@dp.message_handler(commands=['referal'])
async def referal(message: Message):
    userId = message.chat.id
    referals = takeOne(await db.getReferals(userId))
    referalLink = await get_start_link(payload=message.from_user.id)
    referalMessage = f'Ваша реферальная: {referalLink}\nКоличество Ваших рефералов: {referals}'
    await message.answer(referalMessage)


@dp.message_handler(commands=['menu'], state='*')
async def menu(message: Message, state: FSMContext):
    await help_exit(message, state)


@dp.message_handler(commands=['check'], state='*')
async def checkPay(message: Message, state: FSMContext):
    orderId = takeOne(await db.getOrderId(message.chat.id))
    quickpayDict = await db.getQuickpay(orderId)
    redirectedUrl = takeOne(quickpayDict['redirectedUrl'])
    payComment = takeOne(quickpayDict['payComment'])
    await message.answer(f'Заказ #{orderId}', reply_markup=check(redirectedUrl, payComment))
    await state.reset_state()


@dp.message_handler(commands=['adminchat'], state='*')
async def helpChat(message: Message, state:FSMContext):
    await helpToChat(message, state)


@dp.message_handler(commands=['makerchat'], state='*')
async def makerChat(message: Message, state:FSMContext):
    await helpToChat(message, state, 0)


@dp.message_handler(commands=['stopchat'], state='*')
async def stopChat(message: Message, state: FSMContext):
    try:
        userId = message.chat.id
        helpOrderId = takeOne(await db.getHelpOrderIdByUser(userId))
        adminId = takeOne(await db.checkHelpOrderAdmin(helpOrderId))
        await db.EndHelpChat(helpOrderId)
        if adminId != None:
            await state.reset_state()
            await db.setSpareAdmin(adminId)
            await bot.send_message(adminId, 'Чат был завершен пользователем')
            adminState = dp.current_state(chat=adminId, user=adminId)
            await adminState.reset_state()
        else:
            await message.answer('Надеемся мы смогли решить Вашу проблему!)')
    except Exception:
        try:
            adminId = message.chat.id
            helpOrderId = takeOne(await db.getHelpOrderIdByAdmin(adminId))
            userId = takeOne(await db.getHelpUserId(adminId))
            await state.reset_state()
            await db.EndHelpChat(helpOrderId)
            await db.setSpareAdmin(adminId)
            await message.answer('Чат с пользователем завершен!')
            userState = dp.current_state(chat=userId, user=userId)
            await userState.reset_state()
            await bot.send_message(userId, 'Чат был завершен администратором)')
        except:
            pass


@dp.message_handler(commands=['profile'], state='*')
async def getProfile(message: Message, state: FSMContext):
    userId = message.chat.id
    status = None
    orders = None
    allMakers = await db.getAllMakers()
    msg = None
    markup = None
    for maker in allMakers:
        if maker[0] == userId:
            makerId = userId
            username = message.from_user.username
            boolMakerStatus = takeOne(await db.getStatusMaker(makerId))
            if boolMakerStatus == 0:
                makerStatus = Config.changeS[0]
            else:
                makerStatus = Config.changeS[1]
            status = f"Ваш статус: {makerStatus}"
            orders = takeOne(await db.getEndOrdersByMaker(makerId))
            allOrders = f"Завершенных заказов: {orders}"
            msg = f"Вы: {username}\n{status}\n{allOrders}\n"
            await message.answer(msg, reply_markup=await changeStatus(makerId))
    if (status == None) and (orders == None):
        username = message.from_user.username
        orders = takeOne(await db.getEndOrdersByUser(userId))
        msg = f"Вы: @{username}\nКоличество заказов: {orders}\n"
        await message.answer(msg, reply_markup=courseInline)


@dp.message_handler(commands=['update'], state='*')
async def updateMessage(message: Message, state: FSMContext):
    userId = message.chat.id
    allAdmins = list()
    for item in await db.getAllAdmins():
        for maker in item:
            allAdmins.append(maker)
    if userId in allAdmins:
        mess = """Доброго времени суток!
Наш бот обновился! Ждем Вас с новыми заказами!
Если у Вас возникли проблемы пишите - @prograin
Если у Вас есть пожелания к работе бота пишите - @prograin"""
        users = list()
        for item in await db.getAllUsers():
            for user in item:
                users.append(user)
        for user in users:
            try:
                await bot.send_message(user, mess, reply_markup=subjects())
            except:
                pass
    else:
        await message.answer("У Вас недостаточно прав")


@dp.message_handler(commands=['commands'], state='*')
async def allCommands(message: Message, state: FSMContext):
    commands = await bot.get_my_commands()
    mess = "Все команды бота:"
    for command in commands:
        mess += f"\n/{command['command']} - {command['description']}"
    await message.answer(mess)