import asyncio

from aiogram.dispatcher import FSMContext

from bot import dp, bot
from config import Config
from aiogram.types import LabeledPrice, ContentType
from aiogram.dispatcher.filters import Command
from aiogram.types import Message, CallbackQuery, PreCheckoutQuery, ShippingQuery, ShippingOption, InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData
from aiogram.dispatcher.filters import Text

from markups import user_actions
# from markups.keyboards import exit_chat
from services import DataBase, MongoData, setMessages, saveMessages
from services import helpRedirect, takeOne
from states import ModerChat

db = DataBase('exerDB.sql')
mongoPassword = takeOne(db.getMongoPass())
mDb = MongoData(mongoPassword)

# callback
@dp.callback_query_handler(lambda call: call.data.find('order#') == 0, state='*')
async def choose_order(call: CallbackQuery):
    orderId = call.data.split('#')[1]
    maker = call.data.split('#')[-1]
    userId = call.message.chat.id
    await helpRedirect(userId, orderId, 0, maker)


# в колбеке укажи номер ордера чтобы потом его можно было получить
# надо доделать и сделать рассылку всем свободным админам
@dp.callback_query_handler(lambda call: call.data.find('help#') == 0, state='*')
async def messageToAdm(call: CallbackQuery):
    userId = call.message.chat.id
    messageId = call.message.message_id
    if call.data[5] == '1': # если юзер хочет начать чат, то
        await bot.edit_message_text(chat_id=userId, message_id=messageId - 1,
                                    text='Опишите Вашу проблему, Вы были перенаправлены администратору')
        await bot.edit_message_reply_markup(chat_id=userId, message_id=messageId, reply_markup=None)
        await bot.delete_message(chat_id=userId, message_id=messageId)
        await bot.delete_message(chat_id=userId, message_id=messageId + 1)
        adminId = 0
        helpOrderId = 0
        orderId = call.data.split('#')[2]
        if call.data.split('#')[-1] == "0":
            found = False
            while not found:
                try:
                    adminId = takeOne(await db.getSpareAdmin())
                    found = True
                except: # получаем свободного админа
                    await asyncio.sleep(3)
            await helpRedirect(adminId, orderId, 1) # админу отправляем сообщение об обращении с кнопкой о начале чата
            await db.createHelpOrder(userId, orderId)
            helpOrderId = takeOne(await db.getHelpOrderIdByOrderId(orderId))
            await call.message.answer('Для завершения чата с администратором /stopchat')
        if call.data.split('#')[-1] == "1":
            makerId = takeOne(await db.getMakerByOrder(orderId))
            await helpRedirect(makerId, orderId, 1)
            await db.createHelpOrder(userId, orderId)
            helpOrderId = takeOne(await db.getHelpOrderIdByOrderId(orderId))
            await call.message.answer('Для завершения чата с исполнителем /stopchat')
        await mDb.createOrderMessage(helpOrderId, orderId, userId)
    else:
        await call.message.edit_reply_markup(reply_markup=None)
        await call.message.delete()
        await bot.delete_message(chat_id=userId, message_id=messageId + 1)
        await bot.edit_message_text(chat_id=userId, message_id=messageId - 1, text='В следующий раз!)')


@dp.message_handler(state=ModerChat.setMessages)
async def takeMessages(message: Message, state: FSMContext):
    userId = message.chat.id
    text = message.text
    await saveMessages(userId, text)


# по кнопке получаем ордер и админа также отсылаем все сообщения юзера админу
@dp.callback_query_handler(lambda call: call.data.find('helpChat#') == 0)
async def ModerStart(call: CallbackQuery):
    messageId = call.message.message_id
    adminId = call.message.chat.id
    if call.data.split('#')[1] == '1':
        orderId = call.data.split('#')[-1]
        helpOrderId = takeOne(await db.getHelpOrderIdByOrderId(orderId))
        helpOrderStatus = takeOne(await db.getHelpOrderStatus(helpOrderId))
        if helpOrderStatus != 1:
            await call.message.answer('Начат чат с пользователем')
            await db.updateHelpOrder(adminId, orderId)
            allAdmins = list()
            for item in await db.getAllAdmins():
                for maker in item:
                    allAdmins.append(maker)
            if adminId in allAdmins:
                await mDb.setAdmin(helpOrderId, adminId)
                await db.setActiveAdmin(helpOrderId, adminId)
            userId = takeOne(await db.getHelpUserId(adminId))
            messages = await mDb.getMessages(helpOrderId)
            for mess in messages.values():
                if mess != "":
                    for text in mess.values():
                        await call.message.answer(text)
            await ModerChat.chat.set()
            state = dp.current_state(chat=userId, user=userId)
            await state.set_state(ModerChat.userChat)

        else:
            await call.message.answer('Пользователь завершил чат)')
            await bot.delete_message(chat_id=adminId, message_id=messageId)
            await bot.delete_message(chat_id=adminId, message_id=messageId + 1)
            await bot.delete_message(chat_id=adminId, message_id=messageId + 2)

# admin chat
@dp.message_handler(state=ModerChat.chat, content_types=['text', 'document', 'photo'])
async def adminChat(message: Message, state: FSMContext):
    adminId = message.chat.id
    userId = takeOne(await db.getHelpUserId(adminId))
    mess = ""
    if message.text:
        text = message.text
        mess = text
        await bot.send_message(userId, text)
    if message.document:
        fileId = message.document.file_id
        mess = fileId
        await bot.send_document(userId, fileId)
    if message.photo:
        fileId = message.photo[-1].file_id
        mess = fileId
        await bot.send_photo(userId, fileId)
    await saveMessages(userId, mess)


# user chat
@dp.message_handler(state=ModerChat.userChat, content_types=['text', 'document', 'photo'])
async def userChat(message: Message, state: FSMContext):
    userId = message.chat.id
    adminId = takeOne(await db.getHelpAdminId(userId))
    mess = ""
    if message.text:
        text = message.text
        mess = text
        await bot.send_message(adminId, text)
    if message.document:
        fileId = message.document.file_id
        mess = fileId
        await bot.send_document(adminId, fileId)
    if message.photo:
        fileId = message.photo[-1].file_id
        mess = fileId
        await bot.send_photo(adminId, fileId)
    await saveMessages(userId, mess)
