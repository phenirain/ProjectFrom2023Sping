from aiogram.dispatcher import FSMContext

from bot import dp, bot
from config import Config
from aiogram.types import LabeledPrice, ContentType
from aiogram.dispatcher.filters import Command
from aiogram.types import Message, CallbackQuery, PreCheckoutQuery, ShippingQuery, ShippingOption, InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData
from aiogram.dispatcher.filters import Text

from services import DataBase, takeOne
from markups import subjectsInline, user_actions, grades, timeMarkup, courseInline
from services import help_exit
from states import Subject, Maker


db = DataBase('exerDB.sql')


# simple
async def eval(call, userId, state):
    if call.data.split('#')[0] == 'action-1':
        await call.message.delete()
        await bot.send_message(userId, 'Извините, но назначенный исполнитель не сможет выполнить Ваше задание', reply_markup=courseInline)
        await bot.send_message(userId, "Вы можете попробывать заново отправить задание!)")
        orderId = takeOne(await db.getOrderId(userId))
        makerId = takeOne(await db.getMakerByOrder(orderId))
        await db.setSpareMaker(makerId)
        await db.closeState(userId, '1')
    else:
        orderId = takeOne(await db.getOrderId(userId))
        makerId = takeOne(await db.getMakerByOrder(orderId))
        if makerId == None:
            makerId = call.message.chat.id
            await Maker.price.set()
            await call.message.answer('Оцени бро)')
            await db.setActiveMaker(orderId, makerId)
            await db.setActivityMaker(orderId, makerId)
        elif makerId != None:
            messageId = call.data.split('#')[-1]
            await call.message.answer('Этот заказ забрали...')
            await bot.delete_message(chat_id=call.message.chat.id, message_id=messageId)


# message_handler
@dp.message_handler(state=Maker.price)
async def evaluate(message: Message, state: FSMContext):
    makerId = message.chat.id
    price = 0
    try:
        price = int(message.text)
    except ValueError:
        await message.answer('Введите цену')
        await state.reset_state()
        await Maker.price.set()
    userId = takeOne(await db.getUserByMaker(makerId))
    await db.setAmount(userId, price)
    await state.update_data({"price": price})
    await bot.delete_message(makerId, message.message_id - 1)
    await message.answer("Какая длительность исполнения заказа", reply_markup=timeMarkup)


@dp.callback_query_handler(lambda call: call.data.find('time#') == 0, state='*')
async def getTimes(call: CallbackQuery, state: FSMContext):
    times = 0
    match call.data.split('#')[-1]:
        case "0":
            await call.message.answer("Введите длительность исполенения в минутах")
            times = 0
        case "1":
            await call.message.answer("Введите дату и время, до которого Вы выполните задание")
            times = 1
        case "2":
            await call.message.answer("Введите дату, к которой Вы выполните задание")
            times = 2
    await state.update_data({"times" : times})
    await Maker.time.set()
    await call.message.delete()


@dp.message_handler(state=Maker.time)
async def getTime(message: Message, state: FSMContext):
    makerId = message.chat.id
    time = 0
    data = await state.get_data()
    price = data['price']
    times = data['times']
    date = None
    orderId = takeOne(await db.getOrderByMaker(makerId))
    match times:
        case 0:
            try:
                time = int(message.text)
                times = Config.times[times]
            except ValueError:
                await message.answer('Введите время исполнения в минутах: ')
                await state.reset_state(with_data=False)
                await Maker.time.set()
        case 1:
            try:
                timeList = message.text.split(' ')
                date = timeList[0]
                time = timeList[1]
                times = Config.times[times]
            except Exception:
                await message.answer("Введите дату и время в формате 01.01.2001 18:00")
                await state.reset_state(with_data=False)
                await Maker.time.set()
        case 2:
            try:
                timeList = message.text.split('.')
                time = message.text
                times = Config.times[times]
            except Exception:
                await message.answer("Введите дату в формате 01.01.2001")
                await state.reset_state(with_data=False)
                await Maker.time.set()
    await message.answer('Ждем ответа клиента)')
    if date == None:
        message.text = f"Ваша работа будет стоить: {price}P\nРабота будет выполнятся: {times}\n{time} минут"
        await db.setTime(orderId, time)
    else:
        message.text = f"Ваша работа будет стоить: {price}\nРабота будет выполнятся: {times}\nДо {date} {time}"
        await db.setTime(orderId, f"DATE:{date}_TIME:{time}")
    message.reply_markup = user_actions
    userId = takeOne(await db.getUserByMaker(makerId))
    await message.send_copy(userId)
    await state.reset_state()


@dp.message_handler(state=Maker.solve, content_types=['text', 'document', 'photo'])
async def send_file(message: Message, state: FSMContext):
    userId = 0
    makerId = message.chat.id
    if message.text:
        await message.answer('Мне нужен файл)')
    if message.document:
        file_id = message.document.file_id
        userId = takeOne(await db.getUserByMaker(makerId))
        await bot.send_document(userId, file_id)
    if message.photo:
        photo_id = message.photo[-1].file_id
        userId = takeOne(await db.getUserByMaker(makerId))
        await bot.send_photo(userId, photo_id)
    await message.answer("Красава!")
    await db.setSpareMaker(makerId)
    await bot.send_message(userId, 'Оцените пожалуйста!', reply_markup=grades)
    await state.reset_state()


# callback
@dp.callback_query_handler(lambda call: call.data.find('action') == 0)
async def call_hand(call: CallbackQuery, state: FSMContext):
    if call.message.photo:
        try:
            file_id = call.message.photo[-1].file_id
            file_uniq = call.message.photo[-1].file_unique_id
            user_id = takeOne(await db.getUserByPhoto(file_uniq))
            await eval(call, user_id, state)
        except:
            await call.message.delete()
    if call.message.document:
        try:
            file_uniq = call.message.document.file_unique_id
            file_id = call.message.document.file_id
            user_id = takeOne(await db.getUserByFile(file_uniq))
            await eval(call, user_id, state)
        except:
            await call.message.delete()


@dp.callback_query_handler(lambda call: call.data.find('send') == 0)
async def call_file(call: CallbackQuery):
    await call.message.answer('Отправляй!')
    await Maker.solve.set()


@dp.callback_query_handler(lambda call: call.data.find('status') == 0)
async def changeStatus(call: CallbackQuery):
    makerId = call.message.chat.id
    status = int(call.data.split('#')[-1])
    await db.changeMakerStatus(makerId, status)
    await call.message.delete()
    await call.message.answer(f"Теперь Ваш статус: {Config.changeS[status]}")


