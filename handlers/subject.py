import asyncio

from aiogram.dispatcher import FSMContext

from bot import dp, bot
from config import Config
from aiogram.types import LabeledPrice, ContentType, invoice
from aiogram.dispatcher.filters import Command
from aiogram.types import Message, CallbackQuery, PreCheckoutQuery, InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData
from aiogram.dispatcher.filters import Text
import random
from yoomoney import Quickpay, Client

from services import DataBase, help_exit, helpToChat, takeOne
from markups import subjectsInline, check, makerKeyboard, maker_file, courseInline

from states import Subject

db = DataBase('exerDB.sql')


# simple
async def send(message: Message, state: FSMContext, messageId, allMakers):
    # сохраняй file_unique_id а отправляй по file_id
    # print(file_id)  # этот идентификатор нужно где-то сохранить
    msg = message.message_id - 1
    user_id = message.from_id
    subject = takeOne(await db.getSubject(user_id))
    userTime = takeOne(await db.getUserTime(user_id))
    if message.document:
        file_uniq = message.document.file_unique_id
        await db.insertFile(user_id, file_uniq)
        file_id = message.document.file_id
        await db.insertFileId(user_id, file_id)
        for maker in allMakers:
            await bot.send_message(maker, f"Заказ по предмету: {subject}\nВремя выполнения: {userTime}")
            msg = await bot.send_document(maker, file_id, reply_markup=makerKeyboard(messageId + 3))
        await message.answer('Ждем оценки Вашего задания (в среднем до 30м)')
        await state.reset_state()
    elif message.photo:
        file_uniq = message.photo[-1].file_unique_id
        await db.insertPhoto(user_id, file_uniq)
        file_id = message.photo[-1].file_id
        await db.insertFileId(user_id, file_id)
        for maker in allMakers:
            await bot.send_message(maker, f"Заказ по предмету: {subject}\nВремя выполнения: {userTime}")
            await bot.send_photo(maker, file_id, reply_markup=makerKeyboard(messageId + 3))
        await message.answer('Ждем оценки Вашего задания (в среднем до 30м)')
        await state.reset_state()
    elif message.text:
        if message.text == '/help':
            await helpToChat(message, state)
        elif message.text != '/menu':
            await message.answer('Извините, но Вам нужно отправить файл или фото)')
            await state.reset_state()
            await Subject.file.set()
        elif message.text == '/menu':
            await help_exit(message, state)



@dp.message_handler(state=Subject.file, content_types=['text', 'document', 'photo'])
async def say_ref(message: Message, state: FSMContext):
    user_id = message.from_id
    search = True
    await message.answer('Ищем свободных исполнителей...')
    while search:
        makers = await db.getSparesMaker()
        allMakers = list()
        for item in makers:
            for maker in item:
                allMakers.append(maker)
        if len(allMakers) != 0:
            search = False
            await bot.delete_message(chat_id=user_id, message_id=message.message_id + 1)
            await send(message, state, message.message_id, allMakers)
        await asyncio.sleep(5)

# callback
@dp.callback_query_handler(lambda call: call.data.find('user') == 0)
async def user_answer(call: CallbackQuery, state:FSMContext):
    userId = call.message.chat.id
    messageId = call.message.message_id
    if call.data[-1] == '0':
        comment = f'{str(userId)}_{str(random.randint(1000, 9999))}'
        price = await db.getOrderNAmount(call.message.chat.id)
        sum = price[-1][-1]
        quickpay = Quickpay(
            receiver='4100118200745354',
            quickpay_form='shop',
            targets='payment solution',
            paymentType='SB',
            sum=sum,
            label=comment
        )
        orderId = takeOne(await db.getOrderId(userId))
        await db.setQuickpay(orderId, quickpay.redirected_url, comment)
        await call.message.edit_text(text=f'Сформирован заказ номером #{price[-1][0]}\nСтоимость заказа {price[-1][-1]}Р')
        await call.message.edit_reply_markup(reply_markup=check(quickpay.redirected_url, comment))
    else:
        try:
            await call.message.delete()
            await bot.edit_message_text(chat_id=userId, message_id=messageId - 7, text='В следующий раз')
            makerId = takeOne(await db.getMakerByUser(userId))
            await bot.send_message(makerId, "Клиент не согласился(")
        except Exception as e:
            pass
        finally:
            await help_exit(call.message, state)


@dp.callback_query_handler(lambda call: call.data.find('check') == 0)
async def check_pay(call: CallbackQuery):
    userId = call.message.chat.id
    admin = takeOne(await db.getAdmin())
    makerId = takeOne(await db.getMakerByUser(userId))
    p2p = await db.getP2P()
    label = call.data[6:]
    paid_state = await db.checkPaidState(call.message.chat.id)
    paid_state = paid_state[-1][0]
    if paid_state == 0:
        client = Client(p2p[-1][0])
        history = client.operation_history(label=label)
        try:
            operation = history.operations[-1]
            if operation.status == 'success':
                await db.paidState(call.message.chat.id, '1')
                orderId = takeOne(await db.getPaidOrderId(userId))
                time = takeOne(await db.getTime(orderId))
                await call.message.delete()
                await call.message.answer("Ваш платеж прошел успешно!")
                await bot.send_message(admin, f"Платеж прошел успешно по заказу #{orderId}\nДолжен быть выполнен до: {time}")
                await bot.send_message(makerId, f"Заказ #{orderId} оплачен, приступайте к работе! Должен быть выполнен до: {time}",
                                       reply_markup=maker_file)
        except Exception as e:
            orderId = takeOne(await db.getPaidOrderId(userId))
            time = takeOne(await db.getTime(orderId))
            await call.message.answer(Config.wait_pay)
    else:
        orderId = takeOne(await db.getPaidOrderId(userId))
        time = takeOne(await db.getTime(orderId))
        await call.message.delete()
        await call.message.answer("Ваш платеж прошел успешно!")
        await bot.send_message(admin, f"Платеж прошел успешно по заказу #{orderId}\nДолжен быть выполнен до: {time}")
        await bot.send_message(makerId,
                               f"Заказ #{orderId} оплачен, приступайте к работе! Должен быть выполнен до: {time}",
                               reply_markup=maker_file)


@dp.callback_query_handler(lambda call: call.data.find('grade') == 0)
async def call_grade(call: CallbackQuery):
    userId = call.message.chat.id
    await db.setGrade(userId, call.data[-1])
    await db.closeState(userId, '1')
    await call.message.answer('Спасибо за оценку! Ждем Вас снова', reply_markup=courseInline)






