import asyncio

from aiogram.dispatcher import FSMContext

from bot import dp
from config import Config
from aiogram.types import LabeledPrice, ContentType
from aiogram.dispatcher.filters import Command
from aiogram.types import Message, CallbackQuery, PreCheckoutQuery, ShippingQuery, ShippingOption, InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData
from aiogram.dispatcher.filters import Text
from services import DataBase
from markups import subjectsInline, courseInline, userTime
from states import Subject


db = DataBase('exerDB.sql')


@dp.callback_query_handler(lambda call: call.data.find('course') == 0)
async def course(call: CallbackQuery):
    await call.message.edit_text("Выберите срок выполнения Вашей работы!")
    await call.message.edit_reply_markup(reply_markup=subjectsInline(call.data[-1]))


@dp.callback_query_handler(lambda call: call.data.find('subject') == 0)
async def subject(call: CallbackQuery):
    if call.data.split('#')[-1] == '-':
        await call.message.edit_reply_markup(courseInline)
        return
    await call.message.edit_reply_markup(userTime)
    subject = call.data.split('#')[1]
    course = call.data.split('#')[-1]
    userId = call.message.chat.id
    if course == "1":
        await call.message.answer(Config.subjectsData[subject[2:]])
        await db.createOrder(userId, subject[2:])
        return
    await db.createOrder(userId, subject)


@dp.callback_query_handler(lambda call: call.data.find('userTime') == 0)
async def time(call: CallbackQuery):
    await call.message.delete()
    userId = call.message.chat.id
    userTime = call.data.split('#')[-1]
    await db.setUserTime(userId, userTime)
    await call.message.answer(Config.send)
    await Subject.file.set()



