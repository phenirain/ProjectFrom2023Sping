from aiogram.dispatcher import FSMContext
from aiogram.types import Message, InlineKeyboardMarkup, InlineKeyboardButton

from bot import dp, bot

from markups import subjectsInline, grades, courseInline
from services import DataBase, MongoData

from config import Config
from states import ModerChat

def takeOne(sth):
    return sth[-1][0]

db = DataBase('exerDB.sql')
mongoPassword = takeOne(db.getMongoPass())
mDb = MongoData(mongoPassword)


# менюшка с окончанием предыдущего действия
async def help_exit(message: Message, state: FSMContext):
    await state.reset_state()
    userId = message.chat.id
    try:
        orderId = takeOne(await db.getOrderId(userId))
        makerId = takeOne(await db.getMakerByOrder(orderId))
        await db.setSpareMaker(makerId)
        await db.closeState(userId, '1')
    except:
        pass
    finally:
        await message.answer("Меню", reply_markup=courseInline)


# перенаправляем к администратору
async def helpRedirect(user_id, orderId, adm, maker=0):
    orderInfo = await db.getOrderInfo(orderId)
    orderStatus = 'В разработке'
    fileCheck = orderInfo[-1][4]
    if orderInfo[-1][-1] == 1:
        orderStatus = 'Завершен'
    orderMessage = f"""Помощь по заказу номер #{orderInfo[-1][0]}
    Который сейчас находиться в статусе: {orderStatus}
    По предмету: {orderInfo[-1][3]}
    На сумму: {orderInfo[-1][9]}Р
    Оплачен: {'Нет' if orderInfo[-1][-3] == 0 else 'Да'}
    С заданием  ↓ ↓ ↓ ↓ ↓ ↓"""
    if adm == 1:
        markup = InlineKeyboardMarkup()
        markup.insert(InlineKeyboardButton(text='Начать чат', callback_data=f'helpChat#1#{orderId}'))
        await bot.send_message(user_id, orderMessage, reply_markup=markup)
    else:
        markup = InlineKeyboardMarkup()
        markup.insert(InlineKeyboardButton(text='✅Да', callback_data=f'help#1#{orderId}#{maker}'))
        markup.insert(InlineKeyboardButton(text='❌Нет', callback_data=f'help#0#{orderId}#{maker}'))
        await bot.send_message(user_id, orderMessage, reply_markup=markup)
    if fileCheck == None:
        await bot.send_document(user_id, orderInfo[-1][6])
    else:
        await bot.send_photo(user_id, orderInfo[-1][6])
    return orderId


async def setMessages(messages, who, text):
    count = 0
    for key in messages.keys():
        count += 1
    messages[f'{count}'] = {f'{who}': text}
    messages[f'{count + 1}'] = ''
    return messages


async def saveMessages(id, text):
    try:
        helpOrderId = takeOne(await db.getHelpOrderIdByAdmin(id))
        messages = await mDb.getMessages(helpOrderId)
        newMessages = await setMessages(messages, 'admin', text)
        await mDb.newMessages(helpOrderId, newMessages)
    except Exception as e:
        helpOrderId = takeOne(await db.getHelpOrderIdByUser(id))
        messages = await mDb.getMessages(helpOrderId)
        newMessages = await setMessages(messages, 'user', text)
        await mDb.newMessages(helpOrderId, newMessages)
        #ne admin


async def helpToChat(message: Message, state: FSMContext, adm=1):
    orders = InlineKeyboardMarkup()
    callback = "0"
    if adm == 0:
        callback = "1"
    for id in await db.getOrders(message.chat.id):
        orders.insert(InlineKeyboardButton(text=f'#{id[0]}', callback_data=f'order#{id[0]}#{callback}'))
    await message.answer('Выберите заказ, по которому нужна помощь', reply_markup=orders)
    await ModerChat.setMessages.set()




