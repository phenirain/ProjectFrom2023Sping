import pymongo
from pymongo import MongoClient


class MongoData:
    def __init__(self, password):
        cluster = MongoClient(f'mongodb+srv://kutepovvasa:{password}@messages.fzzmgyo.mongodb.net/?retryWrites=true&w=majority')
        self.db = cluster['Messages']
        self.collection = self.db['usersMessages']

    async def createOrderMessage(self, helpOrderId, orderId, userId):
        data = {
            'helpOrderId': helpOrderId,
            'orderId': orderId,
            'userId': userId,
            'adminId': '',
            'messages': {'1': ''}
        }
        self.collection.insert_one(data)

    async def setAdmin(self, helpOrderId, adminId):
        self.collection.update_one({'helpOrderId': helpOrderId}, {"$set": {'adminId': adminId}})

    async def getMessages(self, helpOrderId):
        orderMessages = self.collection.find({'helpOrderId': helpOrderId})
        dictMessage = dict()
        for mess in orderMessages:
            dictMessage = mess['messages']
        return dictMessage

    async def newMessages(self, helpOrderId, updatedMessages):
        self.collection.update_one({'helpOrderId': helpOrderId}, {"$set": {'messages': updatedMessages}})





