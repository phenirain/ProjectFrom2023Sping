import sqlite3




class DataBase:
    def __init__(self, db_file):
        self.connect = sqlite3.connect(db_file)
        self.cursor = self.connect.cursor()

    # creating users, admins and orders
    # async def createUser(self, user_id, user_name, referalId=None):
    #     if referalId != None:
    #         with self.connect:
    #             return self.cursor.execute("INSERT INTO users (user_id, user_name, referalId) VALUES (?, ?, ?)", [user_id, user_name, referalId])
    #     else:
    #         with self.connect:
    #             return self.cursor.execute("INSERT INTO users (user_id, user_name) VALUES (?, ?)", [user_id, user_name])

    # async def createAdmin(self, adminId, adminName):
    #     with self.connect:
    #         return self.cursor.execute("INSERT INTO admins (adminId, adminName) VALUES(?, ?)", [adminId, adminName])

    async def createOrder(self, user_id, subject):
        with self.connect:
            return self.cursor.execute("INSERT INTO orders (user_id, subject) VALUES (?, ?)", [user_id, subject])

    # async def checkAdmin(self, id):
    #     with self.connect:
    #         return self.cursor.execute("SELECT adminName FROM admins WHERE adminId=(?)", [id]).fetchall()

    #find necessary info from config
    def getToken(self):
        with self.connect:
            return self.cursor.execute("SELECT token FROM config").fetchall()

    def getMongoPass(self):
        with self.connect:
            return self.cursor.execute("SELECT mongoPass FROM config").fetchall()

    async def getAdmin(self):
        with self.connect:
            return self.cursor.execute("SELECT admin FROM config").fetchall()

    async def getP2P(self):
        with self.connect:
            return self.cursor.execute("SELECT p2p FROM config").fetchall()

    # users
    async def getReferals(self, referalId):
        with self.connect:
            return self.cursor.execute("SELECT COUNT(user_id) FROM users WHERE referalId=(?)", [referalId]).fetchall()

    # orders
    # set
    # async def insertFile(self, user_id, file_id):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE orders SET file=(?) WHERE user_id=(?) AND close=(?)", [file_id, user_id, 0])
    #
    # async def insertPhoto(self, user_id, photo_id):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE orders SET photo=(?) WHERE user_id=(?) AND close=(?)", [photo_id, user_id, 0])

    # async def insertFileId(self, user_id, file_id):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE orders SET file_id=(?) WHERE user_id=(?) AND close=(?)", [file_id, user_id, 0])
    #
    # async def paidState(self, user_id, ok):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE orders SET paid=(?) WHERE user_id=(?) AND close=(?)", [ok, user_id, 0])

    # async def closeState(self, user_id, close):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE orders SET close=(?) WHERE user_id=(?)", [close, user_id])
    #
    # async def setGrade(self, user_id, grade):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE orders SET grade=(?) WHERE user_id=(?) AND close=(?)", [grade, user_id, 0])
    #
    # async def setAmount(self, user_id, amount):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE orders SET amount=(?) WHERE user_id=(?) AND close=(?)", [amount, user_id, 0])
    #
    # async def setQuickpay(self, orderId, redirectedUrl, payComment):
    #     with self.connect:
    #         self.cursor.execute("UPDATE orders SET redirectedUrl=(?) WHERE id=(?)", [redirectedUrl, orderId])
    #         return self.cursor.execute("UPDATE orders SET payComment=(?) WHERE id=(?)", [payComment, orderId])
    #
    # async def setActiveMaker(self, orderId, makerId):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE orders SET makerId=(?) WHERE id=(?)", [makerId, orderId])

    # async def setTime(self, orderId, times):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE orders SET time=(?) WHERE id=(?)", [times, orderId])
    #
    # async def setUserTime(self, userId, userTime):
    #     with self.connect:
    #         return self.cursor.execute("update orders set userTime=(?) where user_id=(?) and close=0", [userTime, userId])
    # get
    # async def getUserTime(self, userId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT userTime FROM orders WHERE user_id=(?) and close=0", [userId]).fetchall()
    #
    # async def getSubject(self, user_id):
    #     with self.connect:
    #         return self.cursor.execute("SELECT subject FROM orders WHERE user_id=(?) and close=0", [user_id]).fetchall()
    #
    # async def getOrderNAmount(self, user_id):
    #     with self.connect:
    #         return self.cursor.execute("SELECT id, amount FROM orders WHERE user_id=(?) AND close=0", [user_id]).fetchall()
    #
    # async def getOrderByFile(self, file_id):
    #     with self.connect:
    #         return self.cursor.execute("SELECT id FROM orders WHERE file=(?) AND close=(?)", [file_id, 0]).fetchall()

    # async def getUserByFile(self, file_id):
    #     with self.connect:
    #         return self.cursor.execute("SELECT user_id FROM orders WHERE file=(?) AND close=0", [file_id]).fetchall()
    #
    # async def getUserByPhoto(self, photo_id):
    #     with self.connect:
    #         return self.cursor.execute("SELECT user_id FROM orders WHERE photo=(?) AND close=(?)", [photo_id, 0]).fetchall()
    #
    # async def getUserByMaker(self, makerId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT user_id FROM orders WHERE makerId=(?) AND close=(?)", [makerId, 0]).fetchall()
    #
    # async def getMakerByOrder(self, orderId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT makerId FROM orders WHERE id=(?) AND close=(?)", [orderId, 0]).fetchall()

    # async def getMakerByUser(self, userId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT makerId FROM orders WHERE user_id=(?) AND close=(?)", [userId, 0]).fetchall()
    # async def getOrderByMaker(self, makerId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT id FROM orders WHERE makerId=(?) AND close=(?)", [makerId, 0]).fetchall()
    #
    # async def getAmount(self, user_id):
    #     with self.connect:
    #         return self.cursor.execute("SELECT amount FROM orders WHERE user_id=(?) AND close=0", [user_id]).fetchall()
    #
    # async def checkPaidState(self, user_id):
    #     with self.connect:
    #         return self.cursor.execute("SELECT paid FROM orders WHERE user_id=(?) AND close=0", [user_id]).fetchall()
    #
    # async def getOrders(self, user_id):
    #     with self.connect:
    #         return self.cursor.execute("SELECT id FROM orders WHERE user_id=(?) AND (paid=(?) OR close=(?))", [user_id, 1, 0]).fetchall()

    # async def getEndOrdersByMaker(self, makerId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT COUNT(id) FROM orders WHERE makerId=(?) AND paid=(?) AND close=(?)", [makerId, 1, 1]).fetchall()
    #
    # async def getEndOrdersByUser(self, userId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT COUNT(id) FROM orders WHERE user_id=(?) AND paid=(?) AND close=(?)", [userId, 1, 1]).fetchall()

    # async def getOrderId(self, userId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT id FROM orders WHERE user_id=(?) AND close=0", [userId]).fetchall()
    #
    # async def getPaidOrderId(self, userId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT id FROM orders WHERE user_id=(?) AND paid=(?) AND close=(?)", [userId, 1, 0]).fetchall()
    #
    # async def getOrderInfo(self, id):
    #     with self.connect:
    #         return self.cursor.execute("SELECT * FROM orders WHERE id=(?)", [id]).fetchall()
    #
    # async def getQuickpay(self, orderId):
    #     quickpayDict = dict()
    #     with self.connect:
    #         redirectedUrl = self.cursor.execute("SELECT redirectedUrl FROM orders WHERE id=(?)", [orderId]).fetchall()
    #         payComment = self.cursor.execute("SELECT payComment FROM orders WHERE id=(?)", [orderId]).fetchall()
    #     quickpayDict['redirectedUrl'] = redirectedUrl
    #     quickpayDict['payComment'] = payComment
    #     return quickpayDict

    # async def getTime(self, orderId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT time FROM orders WHERE id=(?)", [orderId]).fetchall()

    # helpOrders
    # create
    # async def createHelpOrder(self, userId, orderId):
    #     with self.connect:
    #         return self.cursor.execute("INSERT INTO helpOrders (userId, orderId, orderStatus) VALUES (?, ?, ?)", [userId, orderId, 0])
    #
    # async def updateHelpOrder(self, adminId, orderId):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE helpOrders SET adminId=(?) WHERE orderId=(?) AND orderStatus=(?)", [adminId, orderId, 0])
    #
    # # set and pull
    # async def setHelpOrderStatus(self, OrderId):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE helpOrders SET orderStatus=(?) WHERE orderId=(?)", [1, OrderId])


    # get
    # async def getHelpUserId(self, adminId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT userId FROM helpOrders WHERE adminId=(?) AND orderStatus=(?)", [adminId, 0]).fetchall()
    #
    # async def getHelpAdminId(self, userId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT adminId FROM helpOrders WHERE userId=(?) AND orderStatus=(?)", [userId, 0]).fetchall()
    #
    # async def getHelpOrderIdByOrderId(self, orderId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT id FROM helpOrders WHERE orderId=(?)", [orderId]).fetchall()
    #
    # async def getHelpOrderStatus(self, helpOrderId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT orderStatus FROM helpOrders WHERE id=(?)", [helpOrderId]).fetchall()

    # async def getHelpOrderIdByUser(self, userId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT id FROM helpOrders WHERE userId=(?) AND orderStatus=(?)", [userId, 0]).fetchall()
    #
    # async def getHelpOrderIdByAdmin(self, adminId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT id FROM helpOrders WHERE adminId=(?) AND orderStatus=(?)", [adminId, 0]).fetchall()
    #
    # async def checkHelpOrderAdmin(self, helpOrderId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT adminId FROM helpOrders WHERE id=(?)", [helpOrderId]).fetchall()
    # # ending
    # async def EndHelpChat(self, id):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE helpOrders SET orderStatus=(?) WHERE id=(?)", [1, id])

    # AdminActivity
    # async def addAdmins(self, admins):
    #     registeredAdmins = []
    #     with self.connect:
    #         registeredAdmins = self.cursor.execute("SELECT adminId FROM AdminActivity").fetchall()
    #         registeredAdmins = registeredAdmins[-1]
    #         for admin in admins:
    #             if admin not in registeredAdmins:
    #                 self.cursor.execute("INSERT INTO AdminActivity (adminId) VALUES (?)", [admin])
    #         return self.cursor.execute("SELECT adminId FROM AdminActivity").fetchall()
    #
    # async def setSpareAdmin(self, adminId):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE AdminActivity SET status=(?) AND helpOrderId=(?) WHERE adminId=(?)", [0, 'NULL', adminId])
    #
    # async def getSpareAdmin(self):
    #     with self.connect:
    #         return self.cursor.execute("SELECT adminId FROM AdminActivity WHERE status=(?)", [0]).fetchall()
    #
    # async def setActiveAdmin(self, helpOrderId, adminId):
    #     with self.connect:
    #         self.cursor.execute("UPDATE AdminActivity SET helpOrderId=(?) WHERE adminId=(?)", [helpOrderId, adminId])
    #         return self.cursor.execute("UPDATE AdminActivity SET status=(?) WHERE adminId=(?)", [1, adminId])
    #
    # async def getActiveAdmin(self, orderId):
    #     with self.connect:
    #         return self.cursor.execute("SELECT adminId FROM AdminActivity WHERE orderId=(?) AND status=(?)", [orderId, 0]).fetchall()

    # MakerActivity
    # async def addMaker(self, makerId):
    #     with self.connect:
    #         return self.cursor.execute("INSERT INTO MakerActivity (makerId) VALUES (?)", [makerId])
    #
    # async def getSparesMaker(self):
    #     with self.connect:
    #         return self.cursor.execute("SELECT makerId FROM MakerActivity WHERE makerStatus=(?)", [0]).fetchall()
    #
    # async def setSpareMaker(self, makerId):
    #     with self.connect:
    #         return self.cursor.execute("UPDATE MakerActivity SET makerStatus=(?) WHERE makerId=(?)", [0, makerId])

    async def setActivityMaker(self, orderId, makerId):
        with self.connect:
            self.cursor.execute("UPDATE MakerActivity SET orderId=(?) WHERE makerId=(?)", [orderId, makerId])
            return self.cursor.execute("UPDATE MakerActivity SET makerStatus=(?) WHERE makerId=(?)", [1, makerId])

    async def getActiveMaker(self, orderId):
        with self.connect:
            return self.cursor.execute("SELECT makerId FROM MakerActivity WHERE orderId=(?) AND makerStatus=(?)", [orderId, 0]).fetchall()

    async def getStatusMaker(self, makerId):
        with self.connect:
            return self.cursor.execute("SELECT makerStatus FROM MakerActivity WHERE makerId=(?)", [makerId]).fetchall()

    async def changeMakerStatus(self, makerId, status):
        with self.connect:
            return self.cursor.execute("UPDATE makerActivity SET makerStatus=(?) WHERE makerId=(?)", [status, makerId])



    async def getAllMakers(self):
        with self.connect:
            return self.cursor.execute("SELECT makerId FROM makers").fetchall()

    async def getAllAdmins(self):
        with self.connect:
            return self.cursor.execute("SELECT adminId FROM admins").fetchall()

    async def getAllUsers(self):
        with self.connect:
            return self.cursor.execute("SELECT user_id FROM users").fetchall()

