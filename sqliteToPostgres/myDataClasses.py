from dataclasses import dataclass, asdict


@dataclass
class CommonTable:

    @property
    def getValues(self):
        return '\t'.join([str(x) for x in asdict(self).values()])


@dataclass
class usersS(CommonTable):
    __slots__ = ('user_id', 'user_name', 'referalId')
    user_id: str
    user_name: str
    referalId: str


@dataclass
class makersS(CommonTable):
    __slots__ = ('makerId', 'makerName')
    makerId: str
    makerName: str


@dataclass
class adminsS(CommonTable):
    __slots__ = ("adminId", 'adminName')
    adminId: str
    adminName: str


@dataclass
class configS(CommonTable):
    __slots__ = ('p2p', 'token', 'admin', 'mongoPass')
    p2p : str
    token: str
    admin: str
    mongoPass: str


@dataclass
class AdminActivityS(CommonTable):
    __slots__ = ('status', 'adminId', 'helpOrderId')
    status : str
    adminId : int
    helpOrderId : str


@dataclass
class ordersS(CommonTable):
    __slots__ = ("id", "user_id", "makerId", "subject", "photo", "file", "file_id", "userTime", "time", "amount", "redirectedUrl",
                 "payComment", "paid", "grade", "close")
    id : int
    user_id: int
    makerId: int
    subject: str
    photo : str
    file: str
    file_id : str
    userTime: str
    time: str
    amount: str
    redirectedUrl: str
    payComment: str
    paid: int
    grade: int
    close: int


@dataclass
class helpOrdersS(CommonTable):
    __slots__ = ("id", "adminId", "userId", "orderId", "orderStatus")
    id: int
    adminId: int
    userId: int
    orderId: int
    orderStatus: int


@dataclass
class MakerActivityS(CommonTable):
    __slots__ = ("makerStatus", "makerId", "orderId")
    makerStatus: int
    makerId: int
    orderId: int


@dataclass
class AdminActivityS(CommonTable):
    __slots__ = ("status", "adminId", "helpOrderId")
    status : str
    adminId : int
    helpOrderId: str


