import psycopg2
from psycopg2.extras import DictCursor
from sqliteToPostgres.myDataClasses import usersS, makersS, adminsS, ordersS, helpOrdersS, configS, MakerActivityS, AdminActivityS

host = "127.0.0.1"
user = "phenirain"
password = "pHenR3!)\"!>"
dbName = "mptSolver"


class postgreSQL:
    def __init__(self, host, user, password, dbName):
        dsl = {
            'dbname': dbName,
            'user': user,
            'password': password,
            'host': host,
            'port': '5432',
            'options': '-c search_path=public'
        }
        self.connect = psycopg2.connect(**dsl, cursor_factory=DictCursor)
        self.connect.autocommit = True
        self.cursor = self.connect.cursor()

    def createTable(self, tablesInfo):
        request = ""
        for tableName, dataClass in tablesInfo.items():
            rows = f""
            for row in dataClass.__slots__:
                uniqCheck = (dataClass.__name__ in ['users', 'makers', 'admins'] and row[-2:].lower() == 'id'
                             and row[:-2] != 'referal')
                if row == 'id':
                    rows = f"id serial primary key,"
                    continue
                rows += f"\n{row} {'text' if type(row) == str else 'integer'}{ ' unique' if uniqCheck  else ''},"
            request += f"""create table if not exists {tableName}({rows[:-1]});\n"""
        with self.cursor:
            self.cursor.execute(request)


class users(postgreSQL):
    def refactor(self):
        rename = """
        alter table users
        rename referalid to referal_id;\n"""
        constraints = """
        alter table users
        add constraint uniq_user_id unique(user_id);
        alter table users
        alter column user_name set not null;"""
        request = rename + constraints
        with self.cursor:
            self.cursor.execute(request)

    async def createUser(self, user_id, user_name, referal_id=None):
        if referal_id != None:
            with self.cursor:
                return self.cursor.execute(f"insert into users(user_id, user_name, referal_id) values ('{user_id}', '{user_name}', '{referal_id}')")
        with self.cursor:
            return self.cursor.execute(f"insert into users(user_id, user_name) values('{user_id}', '{user_name}')")

    async def getReferals(self, referal_id):
        with self.cursor:
            self.cursor.execute(f"select count(user_id) from users where referal_id='{referal_id}'")
            return self.cursor.fetchall()

    async def getAllUsers(self):
        with self.cursor:
            self.cursor.execute("select user_id from users")
            return self.cursor.fetchall()

class makers(postgreSQL):

    def refactor(self):
        rename = """
        alter table makers
        rename makerid to maker_id;
        alter table makers
        rename makername to maker_name;\n"""
        constraint = """
        alter table makers
        add constraint uniq_maker_id unique(maker_id);"""
        request = rename + constraint
        with self.cursor:
            self.cursor.execute(request)

    async def getAllMakers(self):
        with self.cursor:
            self.cursor.execute("select maker_id from makers")
            return self.cursor.fetchall()


class admins(postgreSQL):

    def refactor(self):
        rename = """
        alter table admins
        rename adminid to admin_id;
        alter table admins
        rename adminname to admin_name;\n"""
        constraint = """
        alter table admins
        add constraint uniq_admin_id unique(admin_id);
        alter table
        alter column admin_name set not null;"""
        request = rename + constraint
        with self.cursor:
            self.cursor.execute(request)

    async def createAdmin(self, adminId, adminName):
        with self.cursor:
            self.cursor.execute(f"INSERT INTO admins (adminId, adminName) VALUES('{adminId}', '{adminName}');")

    async def checkAdmin(self, adminId):
        with self.cursor:
            self.cursor.execute(f"SELECT adminName FROM admins WHERE adminId='{adminId}';")
            self.cursor.fetchall()

    async def getAllAdmins(self):
        with self.cursor:
            self.cursor.execute("SELECT adminId FROM admins;")
            return self.cursor.fetchall()



class orders(postgreSQL):

    def refactor(self):
        rename = """
        alter table orders
        rename makerid to maker_id;
        alter table orders
        rename usertime to user_time;
        alter table orders
        rename redirectedurl to redirected_url;
        alter table orders
        rename paycomment to pay_comment;
        """

    #set
    async def insertFile(self, user_id, file_id):
        with self.cursor:
            return self.cursor.execute(f"UPDATE orders SET file='{file_id}' WHERE user_id='{user_id}' AND close='0';")

    async def insertPhoto(self, user_id, photo_id):
        with self.cursor:
            return self.cursor.execute(f"UPDATE orders SET photo='{photo_id}' WHERE user_id='{user_id}' AND close='0';")

    async def insertFileId(self, user_id, file_id):
        with self.cursor:
            return self.cursor.execute(f"UPDATE orders SET file_id='{file_id}' WHERE user_id='{user_id}' AND close='0';")

    async def paidState(self, user_id, ok):
        with self.cursor:
            return self.cursor.execute(f"UPDATE orders SET paid='{ok}' WHERE user_id='{user_id}' AND close='0';")

    async def closeState(self, user_id, close):
        with self.cursor:
            return self.cursor.execute(f"UPDATE orders SET close='{close}' WHERE user_id='{user_id}';")

    async def setGrade(self, user_id, grade):
        with self.cursor:
            return self.cursor.execute(f"UPDATE orders SET grade='{grade}' WHERE user_id='{user_id}' AND close='0';")

    async def setAmount(self, user_id, amount):
        with self.cursor:
            return self.cursor.execute(f"UPDATE orders SET amount='{amount}' WHERE user_id='{user_id}' AND close='0';")

    async def setQuickpay(self, order_id, redirected_url, pay_comment):
        with self.cursor:
            request = f"UPDATE orders SET redirected_url='{redirected_url}' WHERE id='{order_id}';\n"
            request += f"UPDATE orders SET pay_comment='{pay_comment}' WHERE id='{order_id}';"
            self.cursor.execute(request)

    async def setActiveMaker(self, order_id, maker_id):
        with self.connect:
            return self.cursor.execute(f"UPDATE orders SET maker_id='{maker_id}' WHERE id='{order_id}';")

    async def setTime(self, order_id, times):
        with self.cursor:
            return self.cursor.execute(f"UPDATE orders SET time='{times}' WHERE id='{order_id}';")

    async def setUserTime(self, user_id, user_time):
        with self.cursor:
            return self.cursor.execute(f"update orders set user_time='{user_time}' where user_id='{user_id}' and close='0';")

    #get
    async def getUserTime(self, user_id):
        with self.cursor:
            self.cursor.execute(f"SELECT user_time FROM orders WHERE user_id='{user_id}' and close='0';")
            return self.cursor.fetchall()

    async def getSubject(self, user_id):
        with self.cursor:
            self.cursor.execute(f"SELECT subject FROM orders WHERE user_id='{user_id}' and close='0';")
            return self.cursor.fetchall()

    async def getOrderNAmount(self, user_id):
        with self.cursor:
            self.cursor.execute(f"SELECT id, amount FROM orders WHERE user_id='{user_id}' AND close='0';")
            return self.cursor.fetchall()

    async def getOrderByFile(self, file_id):
        with self.cursor:
            self.cursor.execute(f"SELECT id FROM orders WHERE file='{file_id}' AND close='0';")
            return self.cursor.fetchall()

    async def getUserByFile(self, file_id):
        with self.cursor:
            self.cursor.execute(f"SELECT user_id FROM orders WHERE file='{file_id}' AND close='0';")


    async def getUserByPhoto(self, photo_id):
        with self.cursor:
            self.cursor.execute(f"SELECT user_id FROM orders WHERE photo='{photo_id}' AND close='0';")
            return self.cursor.fetchall()

    async def getUserByMaker(self, maker_id):
        with self.cursor:
            self.cursor.execute(f"SELECT user_id FROM orders WHERE maker_id='{maker_id}' AND close='0';")
            return self.cursor.fetchall()

    async def getMakerByOrder(self, order_id):
        with self.cursor:
            self.cursor.execute(f"SELECT makerId FROM orders WHERE id='{order_id}' AND close='0';")
            return self.cursor.fetchall()

    async def getMakerByUser(self, user_id):
        with self.cursor:
            self.cursor.execute(f"SELECT makerId FROM orders WHERE user_id='{user_id}' AND close='0';")
            return self.cursor.fetchall()
    async def getOrderByMaker(self, maker_id):
        with self.cursor:
            self.cursor.execute(f"SELECT id FROM orders WHERE makerId='{maker_id}' AND close='0';")
            return self.cursor.fetchall()

    async def getAmount(self, user_id):
        with self.cursor:
            self.cursor.execute(f"SELECT amount FROM orders WHERE user_id='{user_id}' AND close='0';")
            return self.cursor.fetchall()

    async def checkPaidState(self, user_id):
        with self.cursor:
            self.cursor.execute(f"SELECT paid FROM orders WHERE user_id='{user_id}' AND close='0';")
            return self.cursor.fetchall()

    async def getOrders(self, user_id):
        with self.cursor:
            self.cursor.execute(f"SELECT id FROM orders WHERE user_id='{user_id}' AND (paid='1' OR close='0');")
            return self.cursor.fetchall()

    async def getEndOrdersByMaker(self, maker_id):
        with self.cursor:
            self.cursor.execute(f"SELECT COUNT(id) FROM orders WHERE maker_id='{maker_id}' AND paid='1' AND close='1'")
            return self.cursor.fetchall()

    async def getEndOrdersByUser(self, user_id):
        with self.cursor:
            self.cursor.execute(f"SELECT COUNT(id) FROM orders WHERE user_id='{user_id}' AND paid='1' AND close='1'")
            return self.cursor.fetchall()

    async def getOrderId(self, user_id):
        with self.cursor:
            self.cursor.execute(f"SELECT id FROM orders WHERE user_id='{user_id}' AND close='0';")
            return self.cursor.fetchall()

    async def getPaidOrderId(self, user_id):
        with self.cursor:
            self.cursor.execute(f"SELECT id FROM orders WHERE user_id='{user_id}' AND paid='1' AND close='0;")
            return self.cursor.fetchall()

    async def getOrderInfo(self, id):
        with self.cursor:
            self.cursor.execute(f"SELECT * FROM orders WHERE id='{id}';")
            return self.cursor.fetchall()

    async def getQuickpay(self, order_id):
        quickpayDict = dict()
        with self.cursor:
            self.cursor.execute(f"SELECT redirected_url FROM orders WHERE id='{order_id}';")
            redirected_url = self.cursor.fetchall()
        with self.cursor:
            self.cursor.execute(f"SELECT pay_comment FROM orders WHERE id='{order_id}';")
            pay_comment = self.cursor.fetchall()
        quickpayDict['redirectedUrl'] = redirected_url
        quickpayDict['payComment'] = pay_comment
        return quickpayDict

    async def getTime(self, order_id):
        with self.cursor:
            self.cursor.execute(f"SELECT time FROM orders WHERE id='{order_id}';")
            return self.cursor.fetchall()


class help_orders(postgreSQL):

    def refactor(self):
        rename = """
        alter table helpOrders
        rename to help_orders;
        alter table help_orders
        rename adminid to admin_id;
        alter table help_orders
        rename userid to user_id;
        alter table help_orders
        rename orderid to order_id;
        alter table help_orders
        rename orderstatus to order_status;"""
        with self.cursor:
            self.cursor.execute(rename)

    #set
    async def createHelpOrder(self, user_id, order_id):
        with self.cursor:
            self.cursor.execute(f"INSERT INTO help_orders (user_id, order_id, order_status) VALUES ('{user_id}', '{order_id}', '0');")
            return self.cursor.fetchall()

    async def updateHelpOrder(self, admin_id, order_id):
        with self.cursor:
            self.cursor.execute(f"UPDATE help_orders SET admin_id='{admin_id}' WHERE order_id='{order_id}' AND order_status='0';")
            return self.cursor.fetchall()

    async def setHelpOrderStatus(self, order_id):
        with self.cursor:
            self.cursor.execute(f"UPDATE help_orders SET order_status='1' WHERE order_id='{order_id}';")
            return self.cursor.fetchall()

    #get
    async def getHelpUserId(self, admin_id):
        with self.cursor:
            self.cursor.execute(f"SELECT user_id FROM help_orders WHERE admin_id='{admin_id}' AND orderStatus='0';")
            return self.cursor.fetchall()

    async def getHelpAdminId(self, user_id):
        with self.cursor:
            self.cursor.execute(f"SELECT admin_id FROM help_orders WHERE user_id='{user_id}' AND orderStatus='0';")
            return self.cursor.fetchall()

    async def getHelpOrderIdByOrderId(self, order_id):
        with self.cursor:
            self.cursor.execute(f"SELECT id FROM help_orders WHERE order_id=(?);")
            return self.cursor.fetchall()

    async def getHelpOrderStatus(self, help_order_id):
        with self.cursor:
            self.cursor.execute(f"SELECT order_status FROM help_orders WHERE id=(?);")
            return self.cursor.fetchall()

    async def getHelpOrderIdByUser(self, user_id):
        with self.cursor:
            self.cursor.execute(f"SELECT id FROM help_orders WHERE user_id='{user_id}' AND order_status='0';")
            return self.cursor.fetchall()

    async def getHelpOrderIdByAdmin(self, admin_id):
        with self.cursor:
            self.cursor.execute(f"SELECT id FROM help_orders WHERE admin_id='{admin_id}' AND order_status='0';")
            return self.cursor.fetchall()

    async def checkHelpOrderAdmin(self, help_order_id):
        with self.cursor:
            self.cursor.execute(f"SELECT admin_id FROM help_orders WHERE id='{help_order_id}';")
            return self.cursor.fetchall()
    # ending
    async def EndHelpChat(self, id):
        with self.cursor:
            self.cursor.execute(f"UPDATE help_orders SET order_status='1' WHERE id='{id}';")
            return self.cursor.fetchall()

class admin_activity(postgreSQL):

    def refactor(self):
        rename = """
        alter table adminactivity
        rename to admin_activity;
        alter table admin_activity
        rename adminid to admin_id;
        alter table admin_activity
        rename helporderid to help_order_id;"""
        with self.cursor:
            self.cursor.execute(rename)

    async def setSpareAdmin(self, admin_id):
        with self.cursor:
            return self.cursor.execute(f"UPDATE admin_activity SET status='0' AND help_order_id='null' WHERE admin_id='{admin_id}';")

    async def getSpareAdmin(self):
        with self.cursor:
            self.cursor.execute(f"SELECT admin_id FROM admin_activity WHERE status='0';")
            return self.cursor.fetchall()

    async def setActiveAdmin(self, help_order_id, admin_id):
        request = f"UPDATE admin_activity SET help_order_id='{help_order_id}' WHERE admin_id='{admin_id}';\n"
        request += f"UPDATE admin_activity SET status='1' WHERE admin_id='{admin_id}';"
        with self.cursor:
            return self.cursor.execute(request)

    async def getActiveAdmin(self, order_id):
        with self.cursor:
            self.cursor.execute(f"SELECT admin_id FROM admin_activity WHERE order_id='{order_id}' AND status='0';")
            return self.cursor.fetchall()


class maker_activity(postgreSQL):

    def refactor(self):
        rename = """
        alter table makeractivity
        rename to maker_activity;
        alter table maker_activity
        rename makerstatus to maker_status;
        alter table maker_activity
        rename makerid to maker_id;
        alter table maker_activity
        rename orderid to order_id;"""
        with self.cursor:
            self.cursor.execute(rename)

    async def addMaker(self, maker_id):
        with self.cursor:
            return self.cursor.execute(f"INSERT INTO maker_activity (maker_id) VALUES ('{maker_id}');")

    async def getSparesMaker(self):
        with self.cursor:
            self.cursor.execute(f"SELECT maker_id FROM maker_activity WHERE maker_status='0';")
            return self.cursor.fetchall()

    async def setSpareMaker(self, maker_id):
        with self.cursor:
            return self.cursor.execute(f"UPDATE maker_activity SET maker_status='0' WHERE maker_id='{maker_id}';")

    async def setActivityMaker(self, order_id, maker_id):
        with self.cursor:
            request = f"UPDATE maker_activity SET order_id='{order_id}' WHERE maker_id='{maker_id}';"
            request += f"UPDATE maker_activity SET maker_status='1' WHERE maker_id='{maker_id}';"
            self.cursor.execute(request)

    async def getActiveMaker(self, order_id):
        with self.cursor:
            self.cursor.execute(f"SELECT maker_id FROM maker_activity WHERE order_id='{order_id}' AND maker_status='0';")
            return self.cursor.fetchall()

    async def getStatusMaker(self, maker_id):
        with self.cursor:
            self.cursor.execute(f"SELECT maker_status FROM maker_activity WHERE maker_id='{maker_id}';")
            return self.cursor.fetchall()

    async def changeMakerStatus(self, maker_id, status):
        with self.cursor:
            return self.cursor.execute(f"UPDATE maker_activity SET maker_status='{status}' WHERE maker_id='{maker_id}';")


# def select(table, columns, params):
#     with self.cursor:
#         return self.cursor.execute(f"select {''.join(columns)} from {table} ")






if __name__ == "__main__":
    # myTtoC = {
    #     'users': usersS,
    #     'makers': makersS,
    #     'admins': adminsS,
    #     'orders': ordersS,
    #     'helpOrders': helpOrdersS,
    #     'MakerActivity': MakerActivityS,
    #     'AdminActivity': AdminActivityS,
    #     'config': configS,
    # }
    #
    # p = postgreSQL(host, user, password, dbName)
    # p.createTable(myTtoC)
    p = users(host, user, password, dbName)
    print(p.getReferals("1791877710"))



